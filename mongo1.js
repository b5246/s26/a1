//ACTIVITY 1 
use fruits


//Create an aggregate that will get the average of stocks of fruits on sale.
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:null,averageStock:{$avg:"$stock"}}},
	{$project: {_id:0}}
]);



// Get the avaerage stocks of fruits based on specific suppliers.
db.fruits.aggregate([{$group:{_id:"$supplier_id",averageStock:{$avg:"$stock"}}}]);



//PURPOSE
//Create an ave. stock from fruits on sale on specific supplier
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:"$supplier_id",averageStock:{$avg:"$stock"}}},
	{$project: {_id:0}}
]);

