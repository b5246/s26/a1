//ACTIVITY 2 
use fruits


//Create an aggregate that will get the maximum price of fruits on sale.
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:null,maximumPrice:{$max:"$price"}}},
	{$project: {_id:0}}
]);


// Get the maximum price in origin Philippines.
db.fruits.aggregate([
	{$match: {origin:{$in:["Philippines"]}}},
	{$group:{_id:null,maximum_price_philippines:{$max:"$price"}}},
	{$project: {_id:0}}
]);


//PURPOSE
//Get the MAX price of fruits on sale in the origin Philippines
db.fruits.aggregate([
	{$match:
		{$and: [{origin:{$in:["Philippines"]}},{onSale:true}]}},	
	{$group:{_id:null,maximum_price_philippines:{$max:"$price"}}},
	{$project: {_id:0}}
]);
