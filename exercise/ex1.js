let http = require("http");
const port = 4001;

const server = http.createServer((req,res)=>{
	if(req.url == '/welcome'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to the world of Node.js');
	} else if(req.url == '/register'){
		res.writeHead(503, {'Content-Type': 'text/plain'});
		res.end('Page is under Maintenance');
	} else{
		res.writeHead(404,{'Content-Type':'text/plain'});
		res.end('Page not available');
	}
}).listen(port);

console.log(`The server is running at localhost:4001`);